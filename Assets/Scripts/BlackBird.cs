﻿using UnityEngine;

public class BlackBird : Bird {
    [SerializeField]
    public float explosionRadius = 1.3f;
    public CircleCollider2D explosionArea;

    private bool _hasExplosion = false;

    private void FixedUpdate() {
        if (_hasExplosion || state == BirdState.HitSomething) {
            GetComponent<SpriteRenderer>().enabled = false;
            while (true) {
                if (explosionArea.radius >= explosionRadius) break;
                explosionArea.radius += explosionArea.radius;
            }
        }
    }

    public override void OnTap() {
        _hasExplosion = true;
    }
}
