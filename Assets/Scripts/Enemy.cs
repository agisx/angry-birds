﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Enemy : MonoBehaviour{
    public float health = 50f;

    public UnityAction<GameObject> OnEnemyDestroyed = delegate { };

    private bool _isHit = false;

    private void OnDestroy() {
        if (_isHit) OnEnemyDestroyed(gameObject);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.GetComponent<Rigidbody2D>() == null) return;
        if (collision.gameObject.CompareTag("Bird")) {
            _isHit = true;
            Destroy(gameObject);
        } else if (collision.gameObject.CompareTag("Obstacle")) {
            //Hitung damage yang diperoleh
            float damage = collision.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
            health -= damage;
            if (health <= 0) {
                _isHit = true;
                Destroy(gameObject);
            }
        }
    }
}
