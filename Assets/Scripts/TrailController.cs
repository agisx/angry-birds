﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailController : MonoBehaviour
{
    public GameObject trail;
    public Bird targetBird;

    private List<GameObject> _trails;
    void Start()
    {
        _trails = new List<GameObject>();
        
    }
    public void SetBird(Bird bird) {
        targetBird = bird;
        foreach (var trail in _trails) {
            Destroy(trail.gameObject);
        }

        _trails.Clear();
    }

    public IEnumerator SpawnTrail() {
        _trails.Add(Instantiate(trail, targetBird.transform.position, Quaternion.identity));

        yield return new WaitForSeconds(0.1f);

        if(targetBird != null && targetBird.state != Bird.BirdState.HitSomething) {
            StartCoroutine(SpawnTrail());
        }
    }
}
