﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public SlingShooter slingShooter;
    public TrailController trailController;
    
    public List<Bird> birds;
    public List<Enemy> enemies;

    private bool _isGameEnded = false;

    private Bird _shotBird;
    public BoxCollider2D tapCollider;

    // Start is called before the first frame update
    void Start() {
        foreach (var bird in birds) {
            bird.OnBirdDestroyed += ChangeBird;
            bird.OnBirdShot += AssignTrail;
        }

        foreach (var enemy in enemies) {
            enemy.OnEnemyDestroyed += CheckGameEnd;
        }
        tapCollider.enabled = false;
        slingShooter.InitiateBird(birds[0]);
        _shotBird = birds[0];
    }

    public void ChangeBird() {
        tapCollider.enabled = false;

        if (_isGameEnded) return;

        int maxSceneIndex = SceneManager.sceneCount;
        int hasNextScene = SceneManager.GetActiveScene().buildIndex + 1;
        if (hasNextScene <= maxSceneIndex) {
            SceneManager.LoadScene(hasNextScene, LoadSceneMode.Single);
        }

        birds.RemoveAt(0);
        if (birds.Count > 0) { 
            slingShooter.InitiateBird(birds[0]);
            _shotBird = birds[0];
        }
    }

    public void CheckGameEnd(GameObject destroyEnemy) {
        if (enemies.Count == 0) _isGameEnded = true;

        foreach (var enemy in enemies) {
            if(enemy.gameObject == destroyEnemy) {
                enemies.RemoveAt(enemies.IndexOf(enemy));
                break;
            }
        }
    }

    public void AssignTrail(Bird bird) {
        trailController.SetBird(bird);
        StartCoroutine(trailController.SpawnTrail());
        tapCollider.enabled = true;
    } 
    void OnMouseUp() {
        if (_shotBird != null) _shotBird.OnTap();
    }
}
