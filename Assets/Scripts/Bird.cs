﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Bird : MonoBehaviour{
    public enum BirdState { Idle, Thrown, HitSomething }
    public GameObject parent;
    public Rigidbody2D rigidbody2D;
    public CircleCollider2D collider;

    private BirdState _state;
    public BirdState state { get { return _state; } }
    private float _minVelocity = 0.05f;
    private bool _flagDestroy = false;

    public UnityAction OnBirdDestroyed = delegate { }; 
    public UnityAction<Bird> OnBirdShot = delegate { }; 

    void Start(){
        rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
        collider.enabled = false;
        _state = BirdState.Idle; 
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        _state = BirdState.HitSomething;
    }

    private void FixedUpdate() {
        if (_state == BirdState.Idle && rigidbody2D.velocity.sqrMagnitude >= _minVelocity) {
            _state = BirdState.Thrown;
        }
        if ((_state == BirdState.Thrown || _state == BirdState.HitSomething) &&
            rigidbody2D.velocity.sqrMagnitude < _minVelocity && !_flagDestroy) {
            //Hancurkan gameobject setelah 2 detik
            //jika kecepatannya sudah kurang dari batas minimum
            _flagDestroy = true;
            StartCoroutine(DestroyAfter(2));
        } 
    }

    private void OnDestroy() {
        if (_state == BirdState.Thrown || _state == BirdState.HitSomething) OnBirdDestroyed();
    }

    private IEnumerator DestroyAfter(float Second) {
        yield return new WaitForSeconds(Second);
        Destroy(gameObject);
    }

    public virtual void OnTap() { 
    }

    public void MoveTo(Vector2 target, GameObject parent) {
        gameObject.transform.SetParent(parent.transform);
        gameObject.transform.position = target;
    }

    public void Shoot(Vector2 velocity, float distance, float speed) {
        collider.enabled = true;
        rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
        rigidbody2D.velocity = velocity * distance * speed;
        OnBirdShot(this);
    }
}
