﻿using UnityEngine;

public class YellowBird : Bird
{
    [SerializeField]
    public float _boostForce = 100;
    public bool _hasBoost = false;

    public void Boost() {
        if(state == Bird.BirdState.Thrown && !_hasBoost) {
            rigidbody2D.AddForce(rigidbody2D.velocity * _boostForce);
            _hasBoost = true;
        }
    }

    public override void OnTap() {
        Boost();
    }

}
